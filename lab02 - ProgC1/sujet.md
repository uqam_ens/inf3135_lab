# Labo 2: Programmation en C

*Remarque*: Bien que ce laboratoire porte sur la programmation en C, vous
devriez versionner vos solutions à l'aide de Git et les pousser sur un dépôt
GitLab personnel. Cela vous permettra de pratiquer les commandes Git en
alternance avec la programmation, une habitude essentielle que vous devez
prendre quand vous faites du développement logiciel.

## 1 - Arguments de la fonction `main` (45 minutes)

Écrivez un programme `main.c` qui prend exactement un argument et qui affiche
la longueur de cet argument (en tant que chaîne de caractères) sur la sortie
standard. Votre programme doit retourner `0` lorsque tout s'est déroulé
correctement. Écrivez également un fichier `Makefile` qui vous permet de
compiler votre programme. Par exemple, on s'attend au comportement suivant:

```sh
$ make
gcc -o main main.c
$ ./main alpha
"alpha" est de longueur 5
$ ./main "beta 2"
"beta 2" est de longueur 6
$ echo $?
0
```

Si le nombre d'arguments est incorrect, votre programme doit afficher un
message d'erreur sur la sortie d'erreur (`stderr`) et retourner le code
d'erreur `1`:

```sh
$ ./main
Erreur: le programme prend exactement un argument
$ ./main 2> /dev/null
$ ./main beta 2
Erreur: le programme prend exactement un argument
$ echo $?
1
```

*Remarques*:

* Dans Unix, la variable `$?` contient le code d'erreur retournée par la
  dernière commande.
* L'expression `2> /dev/null` redirige la sortie d'erreur vers `/dev/null`, un
  périphérique qui supprime toutes les données qui y sont écrites (autrement
  dit, cela permet de taire le canal d'erreur)
* Pour calculer la longueur d'une chaîne de caractères, la fonction `strlen` de
  la bibliothèque `string.h` vous sera utile.
* Pour afficher sur la sortie d'erreur, on écrit `fprintf(stderr` au lieu de
  `printf(`.

## 2 - Tableaux et structures (60 minutes)

Dans cet exercice, on s'intéresse à la représentation d'un jeu de 52 cartes et
de certaines mains de 5 cartes qu'on peut former au Poker. Voir la [page
Wikipedia correspondante](https://fr.wikipedia.org/wiki/Main_au_poker) pour la
terminologie (*couleur*, *valeur* ou *rang*, *carré*, *main pleine*, *suite*).

Récupérez le fichier [carte.c](carte.c) et complétez l'implémentation des
fonctions

```c
void afficher_couleur(enum couleur c);
void afficher_valeur(valeur v);
void afficher_carte(struct carte carte);
void afficher_main(main5 m);
void afficher_cartes(void);
void compter_couleurs(main5 m, unsigned int num_couleurs[]);
void compter_valeurs(main5 m, unsigned int num_valeurs[]);
bool est_couleur(main5 m);
bool est_carre(main5 m);
bool est_main_pleine(main5 m);
bool est_suite(main5 m);
```

de sorte qu'on ait le comportement suivant:

```c
$ gcc -o carte carte.c
$ ./carte
as de coeur
2 de coeur
3 de coeur
4 de coeur
[...]
valet de pique
dame de pique
roi de pique
mains[0] = (as de carreau,2 de carreau,8 de carreau,valet de carreau,roi de carreau)
Est-ce une suite? non
Est-ce une couleur? oui
Est-ce une main pleine? non
Est-ce un carré? non
mains[1] = (2 de carreau,2 de trèfle,8 de coeur,2 de coeur,2 de pique)
Est-ce une suite? non
Est-ce une couleur? non
Est-ce une main pleine? non
Est-ce un carré? oui
mains[2] = (6 de pique,7 de trèfle,10 de coeur,9 de pique,8 de carreau)
Est-ce une suite? oui
Est-ce une couleur? non
Est-ce une main pleine? non
Est-ce un carré? non
mains[3] = (roi de pique,9 de trèfle,roi de coeur,9 de pique,9 de carreau)
Est-ce une suite? non
Est-ce une couleur? non
Est-ce une main pleine? oui
Est-ce un carré? non
```

## 3 - Affichage de nombres en lettres (60 minutes)

Écrivez un programme `nombre.c` qui affiche en lettres (en français) les
nombres de 0 à 99 sur `stdout` (un nombre par ligne).

À la fin, on s'attend au comportement suivant (le résultat complet se trouve
dans le fichier [nombre.txt](nombre.txt)):

```sh
$ gcc -o nombre nombre.c
$ ./nombre
zéro
un
deux
trois
[...]
quatre-vingt-dix-sept
quatre-vingt-dix-huit
quatre-vingt-dix-neuf
```

Certaines particularités de la langue française doivent être [prises en
considération](https://fr.wikipedia.org/wiki/Nombres_en_fran%C3%A7ais) avant
d'écrire le programme:

1. Les nombres composés contiennent un trait d'union: *dix-sept*,
   *trente-trois*, *quatre-vingt-dix*, etc.
2. Lorsque le chiffre des unités est `1`, on remplace le trait d'union par la
   conjonction *et*: *quarante et un*, *soixante et onze*, etc.
3. La seule exception à la règle précédente est *quatre-vingt-onze*, où on
   utilise un trait d'union plutôt que la conjonction *et*.
4. Le nombre `80` s'accorde au pluriel (*quatre-vingts*), mais pas les nombres
   suivantes: *quatre-vingt-un*, *quatre-vingt-deux*, etc.

Dans une perspective de maintenance, votre programme doit être factorisé le
plus possible, en décomposant le code en fonctions effectuant des tâches
spécifiques. Par exemple, une solution possible consiste à utiliser le type et
les fonctions suivantes:

```c
// Conjonction
enum conj {
    AUC, // Aucune conjonction
    ET,  // et
    TU   // Trait d'union
};
// Affiche l'unité u
void afficher_unite(unsigned int u);
// Affiche la dizaine d
void afficher_dizaine(unsigned int d);
// Affiche un nombre entre 10 et 19
void afficher_10_a_19(unsigned int n);
// Affiche la conjonction 
void afficher_conjonction(enum conj c);
// Affiche un nombre entre 0 et 69
void afficher_0_a_69(unsigned int n);
// Affiche un nombre entre 70 et 79
void afficher_70_a_79(unsigned int n);
// Affiche un nombre entre 80 et 89
void afficher_80_a_89(unsigned int n);
// Affiche un nombre entre 90 et 99
void afficher_90_a_99(unsigned int n);
// Affiche un nombre entre 0 et 99
void afficher_0_a_99(unsigned int n);
```

*Note*: En Belgique et en Suisse, *soixante-dix* et *quatre-vingt-dix* sont
remplacés respectivement par *septante* et *nonante*. De plus, en Suisse,
*quatre-vingts* est remplacé par *huitante* ou *octante*. Le programme serait
donc plus facile à écrire selon ces conventions, mais il est demandé ici de
suivre la convention utilisée en France et au Canada.

*Question optionnelle*: Étendez votre programme pour afficher les nombres
0 à 999999.
